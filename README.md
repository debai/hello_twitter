A quick guide to pulling tweets using twitter's api V1.1. This is guide is primarily meant for undergrads who need/want to pull tweets for one reason or another

What twitter_pull.py accomplishes is given a list of twitter handles in the users.csv file, the script pulls the 3200 most recent tweets for each of those users.

In this git, there are a lot of comments and the code isn't structured properly (no requirements file, etc), this was more written as a guide to help with understanding for beginners so if you're more experienced, please bear with me.

The api endpoints discussed are NOT discussed in full detail. There is a lot more to them, please see the official Twitter docs for more info

Also see license for legal details.
