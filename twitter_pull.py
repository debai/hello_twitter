"""
A quick guide to pulling tweets using twitters api V1.1
This is guide is primarily meant for undergrads who need/want to pull tweets for one reason or another

What the code accomplishes is given a list of twitter handles in the users.csv file, the script pulls the 3200 most recent
tweets for each of those users.

In this file, there are a lot of comments and the code isn't structured properly, this was more written as a guide to help
with understanding for beginners so if you're more experienced, please bear with me

The api endpoint discussed below are NOT discussed in full detail. There is a lot more to them, please see the official
Twitter docs for more info

"""


########################################################################################################################
# Packages / Auth / Urls

# to install packages open up terminal (mac/linux) or powershell (windows) the type: pip install package_name
import requests  # you may need to install the requests package
from requests_oauthlib import OAuth1  # you may need to install the requests-oauthlib package (note the hyphen when you do pip install)
import json  # you may need to install the json package
import pandas as pd  # you may need to install the pandas package


TEST_URL = 'https://api.twitter.com/1.1/account/verify_credentials.json'
URL = "https://api.twitter.com/1.1/statuses/user_timeline.json?"


# see the credentials.png file to see what creds are required below,
# the url for the screenshot above is https://developer.twitter.com/en/apps/
# note the creds below don't actually work
YOUR_APP_KEY = ""
YOUR_APP_SECRET = ""
USER_OAUTH_TOKEN = ""
USER_OAUTH_TOKEN_SECRET = ""

# the OAuth1 package takes care of the authentication after you pass it creds
AUTH = OAuth1(YOUR_APP_KEY, YOUR_APP_SECRET, USER_OAUTH_TOKEN, USER_OAUTH_TOKEN_SECRET)


########################################################################################################################
# Testing if you have api access to twitter

request = requests.get(TEST_URL, auth=AUTH)  # making a http "GET" request. We are hitting the test url using the AUTH credentials

print("Your api account credentials work if you see your account details below:")
print(request.content)
print("\b")
print("\b")
print("###############################################################################################################")


########################################################################################################################
# Getting the usernames that we want to harvest tweets for
# the csv file should be called users.csv and have a column called

df = pd.read_csv("users.csv")

screen_names = list(df["screen_name"])  # only taking the "screen_name" column from the csv
screen_names = list(filter(lambda x: x == x, screen_names))  # taking out the blank rows, nans fail the == check

print("screen_names to harvest from:")
print(screen_names)
print("\b")
print("\b")
print("###############################################################################################################")


########################################################################################################################
# Pulling Tweets

URL = "https://api.twitter.com/1.1/statuses/user_timeline.json?"  # written here again for easy reference
# notes:
# documentaion: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/timelines/api-reference/get-statuses-user_timeline
# the url api endpoint gives up to a max of 3200 of a users most recent tweets.
# the tweets are in chronological order
# Because you can only pull a max of 200 tweets per api call, you have to find the last tweet, get the tweet's id
# Then put that id as a parameter in the next api call so you get tweets only prior to that tweet id specified

# parameters can be added to the url above AFTER the ? at the end
# parameters are separated by "&"
# All of the parameters below are optional as per api documentation
# paramter: screen_name=twitter_handle  # denotes account to pull from, FoodAllergyCAN is a sample twitter handle
# paramter: count=200  # denotes the number of tweets we want from that account, note 200 is the max per call
# paramter: max_id=tweet_id  # denotes a tweet id such that the api returns only tweets prior to that tweet id
# sample completed url: https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=FoodAllergyCAN&count=200

all_tweets = []

for screen_name in screen_names:

    tweet_count = 0
    users_tweets = []

    # First api call for the user hence no max_id parameter
    url = URL + "count=200" + "&screen_name=" + screen_name
    request = requests.get(url=url, auth=AUTH)
    data = json.loads(request.content)  # converts byte string into list/dictionary filled with strings that can be used
    users_tweets += data
    tweet_count += len(data)
    print(screen_name, request)

    if request.status_code >= 400:
        continue

    # Getting the remaining tweets for the user
    while len(data) > 0 and tweet_count < 3200:
        max_id = users_tweets[-1]['id_str']
        max_id = str(int(max_id) - 1)
        url = URL + "count=200" + "&screen_name=" + screen_name + "&max_id=" + max_id
        request = requests.get(url=url, auth=AUTH)
        data = json.loads(request.content)  # converts byte string into list/dictionary filled with strings that can be used
        users_tweets += data
        tweet_count += len(data)

    users_tweets = list(map(lambda x: {**x, **{"screen_name": screen_name}}, users_tweets))  # adds screen name to the row (for the csv)
    all_tweets += users_tweets

    print(screen_name, "tweets: ", tweet_count)  # how many tweets said user had (and that we could pull from)
    print()
# writing to a csv file
df = pd.DataFrame(all_tweets)
df.to_csv("tweets.csv", index=False)









